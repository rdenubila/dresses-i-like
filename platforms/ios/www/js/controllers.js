angular.module('starter.controllers', [])


.controller('geralCrtl', function($scope, $location, $cordovaDialogs, $timeout, Config, User, MoipAppData, Utils) {

	
	$scope.$on('Notificacao', function (event, arg) { 

		data = JSON.parse(arg);
		console.log("Notificação Recebida", data);

		if(data.tipo=="chat"){
			
			$timeout(function(){
				console.log("IR PARA CHAT");
				$location.path("/app/chat/"+data.id);
			}, 1000);
			//$scope.$apply();
		}

		if(data.tipo=="avaliacao"){
			
			$timeout(function(){
				console.log("IR PARA PREFERIDOS");
				$location.path("/app/liked");
			}, 1000);
			//$scope.$apply();
		}

	});

	$scope.MsgLogin = function($msg){

  		if($msg==undefined){
  			$msg = 'Você precisa estar logado para acessar essa área. Deseja logar agora?';
  		}

  		User.Get(function(response){
			$scope.userLogado = response.data;

			console.log($scope.userLogado);

			if($scope.userLogado==undefined){

		  		$cordovaDialogs.confirm($msg, 'Login', ['Sim, quero logar','Não'])
				.then(function(buttonIndex) {
					// no button = 0, 'OK' = 1, 'Cancel' = 2
					if(buttonIndex==1){
						$location.path("/entrar");
					} else if(buttonIndex==2){
						$location.path("/app/home");
					}
				});
			}
  		});
	}

})

.controller('splashCtrl', function($scope, $location, $timeout, Config, User) {

	Config.Load(function(callback) {
	});

	$scope.login = function(){
		User.LoginByToken({
			token: localStorage.getItem('userToken')
		}, function(callback) {
			if(callback.success){

				if(callback.data.confirmado=="1"){
					$location.path("/app/home");
				} else {
					$location.path("/validacao");
				}

			} else {
				$location.path("/instructions");
			}
		});

	}

	if(localStorage.getItem('userToken')){
		$scope.login();
	} else {
		$location.path("/instructions");
	}
  
})


.controller('signInCtrl', function($scope, $location, User, $cordovaCamera, Foto, $cordovaDialogs, $ionicLoading, $window, Utils, $ionicModal, MoipService, MoipAppData, $stateParams, $http) {

	if($stateParams.variacao!=undefined && $stateParams.variacao=="moip"){
		$scope.moip = true;
	} else {
		$scope.moip = false;
	}

	$scope.user = {};
	$scope.termo = false;
	$scope.btnCadastrar = false;


	$scope.back = function(){
		if($scope.moip){
			$window.history.go(-2);
		} else {
			$window.history.back();
		}
	}

	$scope.cadastrar = function(){

		$scope.btnCadastrar = true;
		$ionicLoading.show();
	   
		User.Cadastrar($scope.user, function(callback) {
			console.log(callback);
			if(callback.success){
				$scope.enviarEmailCodigo(callback.id);
			} else {
				$cordovaDialogs.alert(callback.error, "Erro!", "Ok")
				$scope.erro = callback.error;
				$ionicLoading.hide();
			}
			 $scope.btnCadastrar = false;
		});


	}

	$scope.enviarEmailCodigo = function(id){
	   
		Utils.SimpleLoad("jsonSendActivationEmail.php", {id: id} , function(callback) {
			console.log(callback);
			if(callback.success){
				$cordovaDialogs.alert("Seu cadastro foi realizado com sucesso. Você deverá confirmar o seu cadastro com o código enviado para o seu e-mail.", "Cadastro realizado com sucesso!", "Ok")
				$location.path("/login");
			} else {
				$cordovaDialogs.alert(callback.error, "Erro!", "Ok")
				$scope.erro = callback.error;
			}
			$ionicLoading.hide();
		});


	}

	Utils.SimpleLoad("TextoId.php", {id: 1}, function(response){
		$scope.termo = response.data;
	});

	$scope.LerTermo = function(){
		$ionicModal.fromTemplateUrl('templates/modal_termo.html?_', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
		$scope.openModal = function() {
			$scope.modal.show();
		};
		$scope.closeModal = function() {
			$scope.modal.hide();
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});
	}

	$scope.editar = function(){

		data = {
			update: $scope.user
		}

		$scope.btnCadastrar = true;
	   
		User.Alterar(data, function(callback) {

			console.log(callback);
			if(callback.success){
				//$location.path("/validacao");
				$ionicLoading.show({
					template: 'Salvando...'
				})

				User.LoginByToken({
					token: localStorage.getItem('userToken')
				}, function(callback) {
					if(callback.success){
						$location.path("/app/user");
						$ionicLoading.hide();
					} else {
						console.log(callback.error);
					}
				});
			}

		});
	}

	$scope.carregar = function(){
		$ionicLoading.show()
		User.Get(function(response){
			$ionicLoading.hide()
			$scope.user = response.data;
			$scope.user.senha = "";
			$scope.user.alterou_foto = 'n';

			console.log($scope.user);
		}, true)
	}

	$scope.selectImg = function(i){
		$cordovaDialogs.confirm('Selecione a origem da foto', 'Envie uma foto', ['Câmera','Galeria de Fotos'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==1){
				$scope.getCamera(i);
			} else if(buttonIndex==2){
				$scope.getGallery(i);
			}
		});
	}

	$scope.getCamera = function(i){

		var options = Foto.GetOptions();

		$cordovaCamera.getPicture(options).then(function(imageData) {
			$scope.user.foto = "data:image/jpeg;base64," + imageData;
			$scope.user.alterou_foto = 's';
		}, function(err) {
			// error
		});
	}

	$scope.getGallery = function(i){

		var options = Foto.GetOptions();
		options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

		$cordovaCamera.getPicture(options).then(function(imageData) {
			$scope.user.foto = "data:image/jpeg;base64," + imageData;
			$scope.user.alterou_foto = 's';
		}, function(err) {
			// error
		});
	}

	$scope.LogOut = function(){
		User.LogOut(function(response){
			$location.path("/entrar");
			location.reload();
		});
	}

	$scope.checkCEP = function(){
		if($scope.user.cep!=""){

			$ionicLoading.show();

			Utils.GetLongLat($scope.user.cep, function(callback) {

				$ionicLoading.hide();

				console.log(callback);

				if(callback.status=="OK"){
					for(i=0; i<callback.results[0].address_components.length; i++){
						d = callback.results[0].address_components[i];
						if(d.types[0]=="administrative_area_level_1"){
							$scope.user.uf = d.short_name;
						} else if(d.types[0]=="administrative_area_level_2"){
							$scope.user.cidade = d.short_name;
						}
					}
				} else {
					$cordovaDialogs.alert('Digite um CEP válido para a localização do anúncio', 'CEP Inválido', "Ok");
				}
			});
		}
	}


	$scope.ConectarMoip = function(){

		var url = "https://connect.moip.com.br/oauth/authorize?response_type=code";

		url += "&client_id="+MoipAppData.id;
		url += "&redirect_uri=http://www.dressesilike.com.br/Sistema/api/templates/LoginMoip.php";
		url += "&scope=RECEIVE_FUNDS,MANAGE_ACCOUNT_INFO";

		console.log(url);

		try {
			ref = cordova.InAppBrowser.open(url,'_blank', 'location=no');
			ref.addEventListener('loadstart', loadStartCallBack);
		} catch(err) {
			ref = window.open(url,'_blank', 'location=no');
		}

	}

	function loadStartCallBack(params) {
		url = params.url.split("?");
		if(url[0]=="http://www.dressesilike.com.br/close"){
			if(url[1]!=""){
				localStorage.setItem('moipCode', url[1]);
				$scope.GetUserToken();
			}
			$ionicLoading.hide();
			ref.close();
		}
	}


	$scope.GetUserToken = function(){

		$ionicLoading.show();

		var data = {
			code: localStorage.getItem('moipCode'),
			client_id: MoipAppData.id,
			client_secret: MoipAppData.secret,
			grant_type: "authorization_code",
			redirect_uri: MoipAppData.redirectUri
		}

		MoipService.POST_URL("oauth/token", data, function(response){
			console.log("SUCESSO!");
			$scope.SalvarMoip(response.data);
		}, function(response){
			console.log("ERRO!");
			console.log(response);
			$cordovaDialogs.alert('Erro ao conectar sua conta', 'Erro!', "Ok");
		});

	}


	$scope.SalvarMoip = function(data){

		data = {
			update: {
				MoipId: data.moipAccount.id,
				MoipToken: data.accessToken
			}
		}

		User.Alterar(data, function(callback) {
			$scope.carregar();
		});
	}

	$scope.RemoverContaMoip = function(){

		$cordovaDialogs.confirm('Deseja realmente desconectar sua conta Moip?', 'Desconectar conta', ['Não', 'Sim'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==2){

				$ionicLoading.show();

				data = {
					update: {
						MoipId: "",
						MoipToken: ""
					}
				}

				User.Alterar(data, function(callback) {
					$scope.carregar();
				});

			}
		});

		
	}


	$scope.CadastrarMoip = function(){


		var nomeArr = $scope.user.nome.split(" ");

		$scope.userMoip = {
			email: {
				address: $scope.user.email
			},
			person: {
				name: nomeArr[0],
				lastName: nomeArr[1],
				taxDocument: {
					type: "CPF",
					number: ""
				}, 
				birthDate: "",
				phone: {
					countryCode: "55",
					areaCode: "",
					number: $scope.user.celular
				},
				address: {
					street: "",
					streetNumber: "",
					district: "",
					zipCode: $scope.user.cep,
					city: $scope.user.cidade,
					state: $scope.user.uf,
					country: "BRA"
				}
			},
			type: "MERCHANT"
		};


		$ionicModal.fromTemplateUrl('templates/modal_moip_cadastro.html?_'+Math.random(), {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
		$scope.openModal = function() {
			$scope.modal.show();
		};
		$scope.closeModal = function() {
			$scope.modal.hide();
			console.log("close");
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});
	}


	$scope.erroCpf = "";
	$scope.VerificarMoipContaExiste = function(){
		var cpf = $scope.userMoip.person.taxDocument.number;

		if(cpf==undefined){
			$scope.erroCpf = "CPF inválido. Digite um CPF válido para continuar";
			return;
		}

		cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf = cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")


		MoipService.GET("accounts/exists?tax_document="+cpf, function(response){
			console.log("Usuário cadastrado");
			$cordovaDialogs.alert("Este CPF já está cadastrado no Moip. Utilize o botão 'Conectar Conta Moip' para utilizar sua conta com o app Dresses I Like", "Ok");
			$scope.erroCpf = "Este CPF já está cadastrado no Moip. Utilize o botão 'Conectar Conta Moip' para utilizar sua conta com o app Dresses I Like";
		}, function(error){
			console.log("Usuário NÃO cadastrado");
			$scope.erroCpf = "";
			console.error(error);
		});


	}


	$scope.CadastrarMoipAcoes = function(){

		console.log($scope.userMoip);

		var userMoip = angular.copy($scope.userMoip);

		userMoip.person.birthDate = moment( $scope.userMoip.person.birthDate ).format("YYYY-MM-DD");

		userMoip.person.identityDocument.issueDate = moment( $scope.userMoip.person.identityDocument.issueDate ).format("YYYY-MM-DD");

		userMoip.person.phone.areaCode = $scope.userMoip.person.phone.number.substr(0, 2);

		userMoip.person.phone.number = $scope.userMoip.person.phone.number.substr(2);

		$ionicLoading.show();

		MoipService.POST("accounts", userMoip, function(response){
			console.log("SUCESSO!");
			console.log(response.data);

			$ionicLoading.hide();

			$cordovaDialogs.alert("Cadastro realizado com sucesso! Você será redirecionado(a) para uma tela onde deverá criar sua senha. Após criar a senha, volte ao aplicativo e conecte sua conta para poder vender através do Dresses I Like.", "Cadastro MOIP realizado com sucesso!", "Continuar");

			url = response.data._links.setPassword.href;
			console.log(url);
			cordova.InAppBrowser.open(url,'_blank', 'location=no');

			$scope.closeModal();

		}, function(response){
			console.log("ERRO!");
			console.log(response);

			$ionicLoading.hide();
			if(response.errors[0].code=="REG-002"){
				$cordovaDialogs.alert("Você já possui cadastro no MOIP. Conecte sua conta para poder vender através Dresses I Like", "Você já está cadastrado no MOIP!", "Continuar");
			}

			//$scope.closeModal();
		});
	}
  
})



.controller('validateCtrl', function($scope, $location, $timeout, User) {

	User.Get(function(response){
		$scope.user = response.data;
	});

	$scope.erro = "";
	$scope.codigo = {};

	$scope.validar = function(){

		dados = {
			email: $scope.user.email,
			codigo: $scope.codigo.codigo
		}

		console.log($scope.codigo);
		console.log(dados);

		User.Validar(dados, function(callback) {
			console.log(callback);
			if(callback.success){
				$location.path("/app/home");
			} else {
				$scope.erro = callback.error;
			}
		});

	}
  
})


.controller('mainCtrl', function($scope, $window, $cordovaDialogs, $location) {

	$scope.back = function(){
		$window.history.back();
	}
  
  	
})

.controller('instructionsCtrl', function($scope, $ionicSlideBoxDelegate) {

	$scope.instData = [
		{
			img: "img/instructions/img1.jpg",
			titulo: "",
			txt: "Compre vestidos e acessórios novos e usados perto de você!",
			btn: false
		},
		{
			img: "img/instructions/img2.jpg",
			titulo: "",
			txt: "Possui vestidos e acessórios que não usa mais? Venda ou alugue eles aqui!",
			btn: true
		}
	];
	
  
})


.controller('EsqueciSenhaCtrl', function($scope, $location, Utils, $cordovaDialogs, $ionicLoading) {

	$scope.erro = "";
	$scope.sucesso = "";
	$scope.dados = {};

	$scope.Enviar = function(){

		$ionicLoading.show();

		Utils.SimpleLoad("jsonSenhaEmail.php", $scope.dados, function(response){

			$ionicLoading.hide();

			$scope.erro = "";
			$scope.sucesso = "";

			console.log(response);

			if(response.success){
				$scope.sucesso = response.msg;
			} else {
				$scope.erro = response.msg;
			}
		})

	}

})


.controller('loginCtrl', function($scope, $location, User, $cordovaDialogs, $ionicLoading) {

	if(localStorage.getItem('loginData')){
		$scope.dados = JSON.parse(localStorage.getItem('loginData'));
	} else {
		$scope.dados = {};
	}

	$scope.erro = "";

	$scope.login = function(){

		$ionicLoading.show();

		User.Login($scope.dados, function(callback) {

			$ionicLoading.hide();

			if(callback.success){
				localStorage.setItem('loginData', JSON.stringify($scope.dados));
				localStorage.setItem('userToken', callback.data.token);

				if(callback.data.confirmado=="1"){
					$location.path("/app/home");
				} else {
					$location.path("/validacao");
				}

			} else {
				console.log(callback);
				$scope.erro = callback.error;
			}
		});

	}

	$scope.LoginFacebook = function(){
		facebookConnectPlugin.login(["email"], function(response){

			if(response.status=="connected"){

				facebookConnectPlugin.api("me?fields=id,name,email,picture.type(large)", ["email"], $scope.LoginFbSucesso, $scope.LoginFbErro)

			}

		}, $scope.LoginFbErro);
	}

	$scope.LoginFbSucesso = function(response){

		console.log(response);

		 var data = {
			id_facebook: response.id,
			nome: response.name,
			email: response.email,
			foto: response.picture.data.url
		 }

		 $ionicLoading.show();

		 User.LoginFb(data, function(response){
			console.log(response);
			if(response.success){

				localStorage.setItem('userToken', response.token);

				User.LoginByToken({
					token: response.token
				}, function(callback) {

					if(callback.success){
						$location.path("/app/home");
					} else {
						$scope.LoginFbErro(callback);
				   }
				   
				});
			} else {
				$scope.LoginFbErro(response);
			}

			$ionicLoading.hide();

		 });
	}

	$scope.LoginFbErro = function(response){
		$cordovaDialogs.alert('Não foi possível entrar com o Facebook', 'ERRO', "Ok");
		 console.log(response);
	}

	$scope.EsqueciSenha = function(){
		$location.path("/esqueciSenha");
	}
  
})


.controller('appCtrl', function($scope, $rootScope, $location, $state, $cordovaDatePicker, $ionicLoading, $ionicScrollDelegate, $timeout, Config) {

	$scope.class = "";

	Config.Load(function(callback){
		$scope.configData = callback;
	});

	$scope.shadowbox = {
		visible: false,
		url: ""
	}

	$scope.$on('openShadowbox', function (event, arg) { 
		$scope.shadowbox = arg;
	});

	$scope.$on('changeClass', function (event, arg) { 

		$ionicScrollDelegate.scrollTop();
		document.activeElement.blur();
		$timeout(function(){
			document.activeElement.blur();
		}, 1000);

		$scope.class = arg;

		$arrFullHeader = ['user'];
		if(arg!=undefined && $arrFullHeader.indexOf(arg)>=0){
			$scope.hasHeader = true;
		} else {
			$scope.hasHeader = false;
		}
	});

	$scope.SendMsg = function(){
		if($scope.chatMsg!=""){
			$rootScope.$broadcast('SendMsg', $scope.chatMsg);
			$scope.chatMsg = "";
			cordova.plugins.Keyboard.close();
		}
	}

	$scope.hasHeader = false;

	$scope.chatMargem = {'margin-bottom': 0};
	$scope.escondeRodape = false;

	window.addEventListener('native.keyboardshow', keyboardShowHandler);

	function keyboardShowHandler(e){
		$scope.chatMargem = {'margin-bottom': e.keyboardHeight+"px"};
		$scope.escondeRodape = true;
		console.log($scope.chatMargem);
	}


	window.addEventListener('native.keyboardhide', keyboardHideHandler);

	function keyboardHideHandler(e){
		$scope.chatMargem = {'margin-bottom': 0};
		$scope.escondeRodape = false;
	}


	$scope.DeleteChat = function(){
		$rootScope.$broadcast('DeleteChat');
	}


	$scope.checkHome = function(){
		$rootScope.$broadcast('ResetaBusca');
	}
	
  
})


.controller('homeCtrl', function($rootScope, $scope, $location, $cordovaGeolocation, $cordovaDialogs, $ionicLoading, $timeout, Dress, Config, Utils, $ionicPlatform, User) {

	$rootScope.$broadcast('changeClass', "home");

	token = localStorage.getItem('userToken');

	$scope.tipo = 'venda';
	$scope.busca = '';
	$scope.minDist = 100;
	$scope.cards = {};
	$scope.cardsLoading = true;
	$scope.CardsOut = 0;
	$scope.coords = {};
	$scope.animIcon = [];


	$scope.category = {
		visible: false,
		url: ""
	}

	$scope.$on('ResetaBusca', function() {
		$scope.busca = '';
		$scope.loadDresses();
	});
	
	$scope.RegistraDevice = function() {
		User.Get(function(response){
			$scope.user = response.data;
			
			if(window.plugins!=undefined){
				window.plugins.OneSignal.getIds(function(ids) {
					console.log('getIds: ' + JSON.stringify(ids));
					//alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);

					var data = {
						update: {
							OneSignalUserId: ids.userId,
							OneSignalPushToken: ids.pushToken
						}
					}
				   
					User.Alterar(data, function(callback) {
						console.log(callback);
					});
				});
			}

		});
	}

	$scope.changeDressInfo = function(i){

		if($scope.cards.length>i){
			$scope.currentDress = $scope.cards[i];
			$scope.currentDress.cores = $scope.currentDress.cores.split(",");
		} else {
			$scope.showInfo = false;
		}

		//console.log($scope.currentDress);
	}

	$scope.ajustaFiltros = function (){
		cats = Utils.GetCategories();
		$scope.filtro_categorias = cats.length==0 ? "Todas" : cats.length+" selecionadas";

		$scope.filtro_maxDist = Utils.GetMaxDist();

		est = Utils.GetEstConserv();
		if(est.length==0){
			$scope.filtro_estConserv = "Todos";
		} else if(est.length==1){
			for(i=0; i<$scope.configData.conservacao.length; i++){
				if($scope.configData.conservacao[i].id==est[0]){
					$scope.filtro_estConserv = $scope.configData.conservacao[i].nome;
				}
			}
		} else {
			$scope.filtro_estConserv = est.length+" selecionados";
		}
	}

	$scope.loadDresses = function(){

		$ionicLoading.show({
			template: 'Carregando...'
		})

		$scope.cards = {};
		$scope.cardsLoading = true;

		var data = {
			lat: $scope.coords.latitude,
			lng: $scope.coords.longitude,
			cats: Utils.GetCategories(),
			maxDist: Utils.GetMaxDist(),
			estado: Utils.GetEstConserv(),
			tipo: $scope.tipo,
			busca: $scope.busca
		};
		console.log(data);

		Dress.Load(data, function(response){
			if(response.success){

				document.activeElement.blur();

				$scope.showInfo = true;
				$scope.i = 0;
				$scope.CardsOut = 0;

				$scope.cards = response.data;
				$scope.changeDressInfo($scope.i);

				if($scope.cards.length==0){
					$scope.cardsLoading = false;
				}

			} else {
				console.log(response);
			}
			$ionicLoading.hide();
			$scope.RegistraDevice();
		})
	}

	//PEGA A POSICAO DO USUARIO
	var posOptions = {timeout: 10000, enableHighAccuracy: false};


	$ionicPlatform.ready(function() {
		$ionicLoading.show({
			template: 'Localizando...'
		})
	});
	

	$ionicLoading.show({
		template: 'Localizando...'
	})

	$ionicPlatform.ready(function() {
		$cordovaGeolocation.getCurrentPosition(posOptions)
		.then(function (position) {
			$scope.coords  = position.coords;
			$scope.loadDresses();
		}, function(err) {
			$cordovaDialogs.alert("Ative seu GPS para localizar os anúncios mais perto de você!", 'ERRO!', "Ok");
			$ionicLoading.hide();
		});
	});

	// CARREGA CATEGORIAS
	Config.Load(function(callback){
		$scope.configData = callback;
		$scope.ajustaFiltros();
	});

	$scope.openShadowbox = function(link){

		if(link=="categorias"){
			data = {
				visible: true,
				url: 'templates/cat_categoria.html?i=123',
				catsSel: Utils.GetCategories(),
				cats: $scope.configData.categorias,

				showCat: function(index){
					cats = $scope.shadowbox.cats;
					for(i=0; i<cats.length; i++){
						cats[i].visible = i==index;
					}
				},

				initCheckCat: function(opt){
					if($scope.shadowbox.catsSel.length==0){
						return false;
					} else {
						return $scope.shadowbox.catsSel.indexOf(opt.id) >=0;
					}
				},

				saveCats: function(){
					$scope.shadowbox.visible = false;
					var sel = [];
					var qtd = 0;
					//CHECA AS CATEGORIAS MARCADAS E SALVA
					for(i=0; i<$scope.shadowbox.cats.length; i++){
						$sc = $scope.shadowbox.cats[i].subcategorias;
						for(j=0; j<$sc.length; j++){
							qtd++;
							if($sc[j].check){
								sel.push($sc[j].id);
							}
						}
					}
					if(sel.length==qtd){
						sel = [];
					}
					localStorage.setItem('catsSel', sel.join(","));
					$rootScope.$broadcast('closeShadowbox');
				}
			}

		} else if(link=="localizacao"){
			data = {
				visible: true,
				url: 'templates/cat_localizacao.html?i=123',
				maxDist: Utils.GetMaxDist(),

				save: function(){
					$scope.shadowbox.visible = false;
					localStorage.setItem('maxDist', $scope.shadowbox.maxDist);
					$rootScope.$broadcast('closeShadowbox');
				}
			}

		} else if(link=="conservacao"){
			data = {
				visible: true,
				url: 'templates/cat_conservacao.html?i='+Math.random(),
				cats: $scope.configData.conservacao,
				catsSel: Utils.GetEstConserv(),

				isSel: function(opt){
					if($scope.shadowbox.catsSel.length==0){
						return true;
					} else {
						return $scope.shadowbox.catsSel.indexOf(opt.id) >=0;
					}
				},

				save: function(){

					var sel = [];
					var qtd = 0;
					//CHECA AS CATEGORIAS MARCADAS E SALVA
					for(i=0; i<$scope.shadowbox.cats.length; i++){
						if($scope.shadowbox.cats[i].check){
							sel.push($scope.shadowbox.cats[i].id);
						}
						qtd++;
					}
					if(sel.length==qtd){
						sel = [];
					}
					localStorage.setItem('estConserv', sel.join(","));


					$scope.shadowbox.visible = false;
					$rootScope.$broadcast('closeShadowbox');
				}
			}
		}

		$rootScope.$broadcast('openShadowbox', data);

	}

	$scope.$on('closeShadowbox', function (event) {
		$scope.ajustaFiltros();
	});

	$scope.removeCard = function (index){
		i = $scope.cards.length-index-1;
		$scope.cards.splice(i, 1);
		$scope.changeDressInfo($scope.i);
	}


	$scope.throwout = function (eventName, eventObject) {
		$scope.CardsOut++;

		if($scope.CardsOut>=$scope.cards.length){
			$scope.cardsLoading = false;
		}
	};

	$scope.throwoutleft = function (eventName, eventObject) {
		//console.log('throwoutleft', eventObject);
		//$scope.SetLiked("n");

		$scope.i++;
		$scope.changeDressInfo($scope.i);
		$scope.$apply();
	};

	$scope.throwoutright = function (eventName, eventObject) {
		//console.log('throwoutright', eventObject);
		//$scope.SetLiked("s");

		$scope.i++;
		$scope.changeDressInfo($scope.i);
		$scope.$apply();
	};

	$scope.options = {
		throwOutConfidence: function (offset, elementWidth) {
			//console.log('throwOutConfidence', offset, elementWidth);
			return Math.min(Math.abs(offset) / $scope.minDist, 1);
		},
		isThrowOut: function (offset, elementWidth, throwOutConfidence) {
			//console.log('isThrowOut', offset, throwOutConfidence);

			return throwOutConfidence === 1;
		}
	};

	$scope.SetLiked = function(liked){

		if($scope.userLogado==undefined){

			$scope.MsgLogin('Você precisa estar logado para marcar sua preferência. Deseja logar agora?');

			return false;
		}

		$scope.animIcon = [liked];
		$timeout(function(){ $scope.animIcon = []; }, 900);

		data = {
			gostou: liked,
			id_anuncio: $scope.currentDress.id
		}

		Dress.SetLiked(data, function(callback) {
			if(!callback.success){
				console.log(callback);
			}
		});

		//console.log($scope.currentDress);

	}
  
})


.controller('addCtrl', function($rootScope, $scope, $cordovaCamera, Foto, Dress, Utils, Config, $cordovaDialogs, $location, $ionicLoading, $stateParams, $window, User, $ionicModal) {
   

	$scope.configData = {};

	$rootScope.$broadcast('changeClass', "add");

	$scope.addData = {
		tipo: 'venda'
	}


	/*$scope.OpenModalMoip = function(){
		$ionicModal.fromTemplateUrl('templates/modal_moip.html?_', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
		$scope.openModal = function() {
			$scope.modal.show();
		};
		$scope.closeModal = function() {
			$scope.modal.hide();
			console.log("close");

			if($scope.user.email_moip==""){
				$location.path("/app/home");
			}
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});
	}*/

	/*$scope.SalvarMoip = function(){

		$ionicLoading.show();

		data = {
			update: {
				email_moip: $scope.user.email_moip
			}
		}

		User.Alterar(data, function(callback) {
			$ionicLoading.hide();
			console.log(callback);
			if(callback.success){
				$scope.closeModal();
			}
		});
	}*/

	User.Get(function(response){
		$scope.user = response.data;

		if($scope.userLogado==undefined) $scope.MsgLogin();
		
		//Abre o Modal Moip para pagamentos
		/*if($scope.user.MoipId==""){
			//$scope.OpenModalMoip();
			$location.path("/meus_dados/moip");
		}*/
	});

	$scope.selectImg = function(i){
		$cordovaDialogs.confirm('Selecione a origem da foto', 'Envie uma foto', ['Câmera','Galeria de Fotos'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==1){
				$scope.getCamera(i);
			} else if(buttonIndex==2){
				$scope.getGallery(i);
			}
		});
	}

	$scope.getCamera = function(i){

		var options = Foto.GetOptions();

		$cordovaCamera.getPicture(options).then(function(imageData) {
			$scope.addData["img"+i] = "data:image/jpeg;base64," + imageData;
		}, function(err) {
			// error
		});
	}

	$scope.getGallery = function(i){

		var options = Foto.GetOptions();
		options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

		$cordovaCamera.getPicture(options).then(function(imageData) {
			$scope.addData["img"+i] = "data:image/jpeg;base64," + imageData;
		}, function(err) {
			// error
		});
	}

	$scope.checkCEP = function(){
		if($scope.addData.cep!=""){
			Utils.GetLongLat($scope.addData.cep, function(callback) {

				if(callback.status=="OK"){
					$scope.addData.lat = callback.results[0].geometry.location.lat;
					$scope.addData.lng = callback.results[0].geometry.location.lng;
				} else {
					$cordovaDialogs.alert('Digite um CEP válido para a localização do anúncio', 'CEP Inválido', "Ok");
				}
			});
		}
	}

	$scope.enviarAnuncio = function (){

		console.log($scope.addData);

		if(
			$scope.addData.titulo==undefined ||
			$scope.addData.descricao==undefined ||
			$scope.addData.cep==undefined ||
			$scope.addData.valor==undefined ||
			$scope.addData.categoria==undefined ||
			$scope.addData.est_conserv==undefined ||
			$scope.addData.qtd==undefined
		){
			$cordovaDialogs.alert("Preencha todos os dados", "Cadastro incompleto!", "Ok")

		} else if($scope.addData.img0==undefined && $scope.addData.img1==undefined && $scope.addData.img2==undefined){
			$cordovaDialogs.alert("Adicione pelo menos uma foto no seu anúncio", "Cadastro incompleto!", "Ok");

		} else {

			$scope.btnCadastrar = true;
			$ionicLoading.show()

			Dress.Add($scope.addData, function(response){
				console.log(response);
				if(response.success){
					$cordovaDialogs.alert(response.msg, 'Sucesso!', "Ok")
					.then(function(res) {
						$location.path("/app/dress/"+response.id);
						//$window.history.back();
					});
				} else {
					$cordovaDialogs.alert(response.msg, 'ERRO!', "Ok");
				}

				$ionicLoading.hide();
				$scope.btnCadastrar = false;
			});
		}
	}


	$scope.Edit = function(){
		if($stateParams.id!=undefined){
			var data = {
				lat: 0,
				lng: 0,
				id: $stateParams.id
			}

			$ionicLoading.show({
				template: 'Carregando...'
			})

			Dress.LoadInfo(data, function(response){
				if(response.success){

					response.data.qtd = parseInt(response.data.qtd);
					/*response.data.tamanho = response.data.id_tamanho;
					response.data.estado = response.data.id_conservacao;
					response.data.categoria = response.data.id_categoria;*/

					//response.data.id_categoria
					//response.data.categoria

					//PEGA CATEGORIAS
					cat = Utils.SearchInArray($scope.configData.categorias, response.data.id_cat);
					subcat = Utils.SearchInArray(cat.subcategorias, response.data.id_subcat);
					data = {
						cat: {
							id: cat.id,
							nome: cat.nome
						},
						subcat: subcat
					}
					response.data.categoria = data;

					//PEGA CORES
					cores = response.data.cores=="" ? [] : response.data.cores.split(",");
					c = [];
					for(i=0; i<cores.length; i++){
						c.push(Utils.SearchInArray(subcat.cores, cores[i]));
					}
					response.data.cores = c;

					//PEGA TAMANHOS
					tamanhos = response.data.ids_tamanhos.split(",");
					v = [];
					for(i=0; i<tamanhos.length; i++){
						v.push(Utils.SearchInArray(subcat.tamanhos, tamanhos[i]));
					};
					response.data.tamanho = v;

					//PEGA COMPRIMENTO
					response.data.comprimento = Utils.SearchInArray(subcat.comprimento, response.data.id_comprimento);

					 //PEGA CONSERVACAO
					response.data.est_conserv = Utils.SearchInArray($scope.configData.conservacao, response.data.id_conservacao);




					response.data.img0 = response.data.foto[0];
					response.data.img1 = response.data.foto[1];
					response.data.img2 = response.data.foto[2];

					$scope.addData = response.data;
					$scope.addData.tamanho_txt = Utils.GetTextByArray($scope.addData.tamanho);
					//console.log($scope.addData);
				} else {
					console.log(response);
				}
				$ionicLoading.hide();
			})
		}
	}

	$scope.$on('AddSelCat', function (event, cat) {
		$scope.addData.categoria = cat;
		console.log($scope.addData.categoria.subcat.cores);
	});

	$scope.$on('AddSelConserv', function (event, cat) {
		$scope.addData.est_conserv = cat;
	});

	$scope.$on('AddSelComprimento', function (event, cat) {
		$scope.addData.comprimento = cat;
	});

	$scope.$on('AddSelTamanho', function (event, cat) {
		$scope.addData.tamanho = cat;
		$scope.addData.tamanho_txt = Utils.GetTextByArray($scope.addData.tamanho);
	});

	$scope.$on('AddSelCores', function (event, cat) {
		$scope.addData.cores = cat;
	});

	$scope.OpenSelect = function(type){

		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.close();
		}

		if(type=="categorias"){

			data = {
				visible: true,
				url: 'templates/select_categoria.html?i=123',
				cats: $scope.configData.categorias,

				showCat: function(index){
					cats = $scope.shadowbox.cats;
					for(i=0; i<cats.length; i++){
						cats[i].visible = i==index;
					}
				},

				selectCat: function(cat, subcat){

					data = {
						cat: {
							id: cat.id,
							nome: cat.nome
						},
						subcat: subcat
					}

					$scope.shadowbox.visible = false;
					$rootScope.$broadcast('AddSelCat', data);
				}

			}

		} else if(type=="estado"){

			data = {
				visible: true,
				titulo: "Estado de Conservação",
				url: 'templates/select_radio.html',
				opts: $scope.configData.conservacao,

				select: function(opt){
					$scope.shadowbox.visible = false;
					$rootScope.$broadcast('AddSelConserv', opt);
				}

			}

		} else if(type=="comprimento"){

			data = {
				visible: true,
				titulo: "Comprimento",
				url: 'templates/select_radio.html',
				opts: $scope.addData.categoria.subcat.comprimento,

				select: function(opt){
					$scope.shadowbox.visible = false;
					$rootScope.$broadcast('AddSelComprimento', opt);
				}

			}

		} else if(type=="tamanhos"){

			data = {
				visible: true,
				titulo: "Tamanhos",
				url: 'templates/select_checkbox.html',
				opts: $scope.addData.categoria.subcat.tamanhos,

				select: function(){
					$scope.shadowbox.visible = false;

					sel = [];
					for(i=0; i<$scope.shadowbox.opts.length; i++){
						if($scope.shadowbox.opts[i].checked){
							sel.push($scope.shadowbox.opts[i]);
						}
					}

					$rootScope.$broadcast('AddSelTamanho', sel);
				}

			}

		} else if(type=="cores"){

			console.log($scope.addData.categoria.subcat.cores);

			data = {
				visible: true,
				titulo: "Tamanhos",
				url: 'templates/select_checkbox.html',
				opts: $scope.addData.categoria.subcat.cores,

				select: function(){
					$scope.shadowbox.visible = false;

					sel = [];
					for(i=0; i<$scope.shadowbox.opts.length; i++){
						if($scope.shadowbox.opts[i].checked){
							sel.push($scope.shadowbox.opts[i]);
						}
					}

					$rootScope.$broadcast('AddSelCores', sel);
				}

			}

		}
		
		$rootScope.$broadcast('openShadowbox', data);

	}


	//CARREGA AS CONFIGURACOES

	Config.Load(function(callback){
		$scope.configData = callback;
		$scope.Edit();
	});
  
})


.controller('userCtrl', function($rootScope, $scope, $location, $stateParams, Dress, User, Utils, $cordovaDialogs, $ionicLoading, $cordovaDatePicker, $ionicModal) {

	$rootScope.$broadcast('changeClass', "user");

	if($scope.userLogado==undefined) $scope.MsgLogin();

	$scope.tipo = $stateParams.tipo==undefined ? 'anuncios' : $stateParams.tipo;
	$scope.status = 'aberto';

	$scope.user = {}

	User.Get(function(d){
		console.log(d);
		$scope.user = d.data;
	}, true);

	$scope.vestidos = {};

	$scope.selTab = function(tab){
		$scope.status = tab;
		$scope.vestidos = {}
		$scope.LoadDresses();
	}

	$scope.LoadDresses = function(){

		data = {
			status: $scope.status
		}

		Dress.LoadAnuncios(data, function(response){
			if(response.success){
				$scope.vestidos = response.data
			}
		})
	}

	$scope.Finalizar = function(id){

		$cordovaDialogs.confirm('Deseja realmente finalizar este anúncio?', 'Finalizar Anúncio', ['Sim','Não'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==1){

				data = {
					id_anuncio: id,
					update: {
						status: "fechado"
					}
				}

				Dress.UpdateAnuncios(data, function(response){
					if(response.success){
						console.log(response);
						$scope.status = 'fechado';
						$scope.LoadDresses();
					} else {
						console.log(response);
					}
				})

			}
		});
	}

	$scope.Ativar = function(id){

		$cordovaDialogs.confirm('Deseja realmente ativar este anúncio?', 'Ativar Anúncio', ['Sim','Não'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==1){

				data = {
					id_anuncio: id,
					update: {
						status: "aberto"
					}
				}

				Dress.UpdateAnuncios(data, function(response){
					if(response.success){
						console.log(response);
						$scope.status = 'aberto';
						$scope.LoadDresses();
					} else {
						console.log(response);
					}
				})

			}
		});
	}

	$scope.Ver = function(id){
		$location.path("/app/dress/"+id);
	}

	$scope.Editar = function(id){
		$location.path("/app/update/"+id);
	}

	$scope.Qualificar = function(d){

		data = {
			visible: true,
			url: 'templates/select_qualificar.html?i=123',
			avaliacao: 0,
			btnEnviar: false,
			data: d,

			changeAvaliacao: function(i){
				$scope.shadowbox.avaliacao = i;
				$scope.shadowbox.btnEnviar = true;
			},

			save: function(){
				
				$ionicLoading.show();

				data = {
					nota: $scope.shadowbox.avaliacao,
					id_anuncio: $scope.shadowbox.data.id,
					id_vendedor: $scope.shadowbox.data.vendedor.id,
				};

				Utils.SimpleLoad('jsonSetAvaliacao.php', data, function(response){
					$scope.shadowbox.visible = false;
					$ionicLoading.hide();
					$cordovaDialogs.alert("Agradecemos sua avaliação", "Avaliação enviada com sucesso!", "Ok")
				});
			}
		}

		$rootScope.$broadcast('openShadowbox', data);
	}

	$scope.Eventos = function(id){

		data = {
			visible: true,
			url: 'templates/modal_eventos.html?i='+Math.random(),
			id_anuncio: id,
			user_id: $scope.user.id,
			anuncios: [],
			ev: {},

			loadEventos: function(id){
				$ionicLoading.show();
				data = {
					id_anuncio: id
				}
				Utils.SimpleLoad("jsonGetVestidoEventos.php", data, function(response){
					if(response.success){
						$scope.shadowbox.anuncios = response.data;
					}
					$ionicLoading.hide();
				});
			},

			remover: function(id){
				$ionicLoading.show();
				data = {
					tbl: "anuncios_eventos",
					delete: {
						id: id
					}
				}
				Utils.SimpleDelete(data, function(response){
					$scope.shadowbox.loadEventos($scope.shadowbox.id_anuncio);
				});
			},

			cadastrar: function(){
				data = {
					visible: true,
					url: 'templates/modal_eventos_cadastrar.html?i=456',
					id_anuncio: $scope.shadowbox.id_anuncio,
					ev: {},

					selData: function(){
						var options = {
							date: new Date(),
							mode: 'date'
						};
						$cordovaDatePicker.show(options).then(function(date){
							d = moment( date ).format("DD/MM/YYYY");
							$scope.shadowbox.ev.data = d;
						});
					},

					selHora: function(){
						var options = {
							date: new Date(),
							mode: 'time'
						};
						$cordovaDatePicker.show(options).then(function(date){
							d = moment( date ).format("HH:mm");
							$scope.shadowbox.ev.hora = d;
						});
					},

					salvarEvento: function(){

						$ionicLoading.show();

						$scope.shadowbox.ev.id_anuncio = $scope.shadowbox.id_anuncio;

						Utils.SimpleLoad("jsonSetVestidoEvento.php", $scope.shadowbox.ev, function(response){
							if(response.success){
								$scope.shadowbox.visible = false;
								$cordovaDialogs.alert('Evento cadastrado com sucesso!', 'Sucesso!', 'Ok');
							} else {
								console.log(response);
							}

							$ionicLoading.hide();
						})
					}
				};
				console.log(data);
				$rootScope.$broadcast('openShadowbox', data);
			}
		}

		$rootScope.$broadcast('openShadowbox', data);
	}

	$scope.LoadPedidos = function(){

		data = {
			tipo: "pedidos"
		};

		Utils.SimpleLoad('jsonGetPedidos.php', data, function(response){
			if(response.success){
				$scope.pedidos = response.data
			}
		})
	}
	$scope.LoadPedidos();



	$scope.LoadNegociacoes = function(){

		data = {
			tipo: "negociacoes"
		};

		Utils.SimpleLoad('jsonGetPedidos.php', data, function(response){
			if(response.success){
				$scope.negociacoes = response.data;
				console.log($scope.negociacoes);
			}
		})
	}
	$scope.LoadNegociacoes();


	$scope.Config = function(){
		$location.path("/meus_dados");
	}
  
	$scope.selTab('aberto');




	Utils.SimpleLoad("TextoId.php", {id: 2}, function(response){
		$scope.faq = response.data;
	});

	$scope.LerFaq = function(){
		$ionicModal.fromTemplateUrl('templates/modal_faq.html?_', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
		$scope.openModal = function() {
			$scope.modal.show();
		};
		$scope.closeModal = function() {
			$scope.modal.hide();
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});
	}

})


.controller('likedCtrl', function($stateParams, $rootScope, $scope, $http, $location, Dress, $ionicLoading, Utils, $cordovaDialogs) {

	$rootScope.$broadcast('changeClass', "liked");

	if($scope.userLogado==undefined) $scope.MsgLogin();

	$scope.selecionando = false;
	$scope.selAjuda = [];

	$scope.startSelect = function(s){
		$scope.selAjuda = [];
		$scope.selecionando = s;
	}

	$scope.selDress = function($i){
		if($scope.selecionando){
			index = $scope.selAjuda.indexOf($scope.vestidos[$i].id);
			if(index>=0){
				$scope.selAjuda.splice(index, 1);
			} else {
				$scope.selAjuda.push($scope.vestidos[$i].id);
			}

			if($scope.selAjuda.length>2){
				$scope.selAjuda.splice(0, 1);
			}

		} else {
			$location.path("/app/dress/"+$scope.vestidos[$i].id);
		}
	}

	$scope.pedirAjuda = function() {

		if($scope.selAjuda.length<2){
			$cordovaDialogs.alert('Selecione dois anúncios para pedir ajuda', 'Selecione', "Ok");
			return false;
		}

		data = {
			visible: true,
			url: 'templates/modal_amigos.html?i='+Math.random(),
			anuncios: $scope.selAjuda,
			amigos: [],

			loadAmigos: function(){
				$ionicLoading.show();
				data = {
				}
				Utils.SimpleLoad("jsonGetUsuarios.php", data, function(response){
					console.log(response);
					if(response.success){
						$scope.shadowbox.amigos = response.data;
					}
					$ionicLoading.hide();
				});
			},

			save: function(){
				var amigos = [];
				for(i=0; i<$scope.shadowbox.amigos.length; i++){
					a = $scope.shadowbox.amigos[i];
					if(a.sel){
						amigos.push(a.id);
					}
				}

				if(amigos.length<1){
					$cordovaDialogs.alert('Selecione pelo menos 3 amigas para pedir opinião', 'Selecione', "Ok");
					return false;
				}

				$ionicLoading.show();

				data = {
					anuncios: $scope.shadowbox.anuncios,
					amigos: amigos
				}

				Utils.SimpleLoad("jsonSetPedirOpiniao.php", data, function(response){
					console.log(response);
					if(response.success){
						$cordovaDialogs.alert('Seu pedido foi enviado com sucesso!', 'Sucesso!', "Ok");
					} else {
						console.log(response);
					}
					$ionicLoading.hide();
					$scope.shadowbox.visible = false;
				});

			}
		}

		$rootScope.$broadcast('openShadowbox', data);
		$scope.startSelect(false);
	}

	$scope.avaliarSel = function(anuncios, index, id_pedido){
		angular.forEach(anuncios, function(value, key) {
			value.sel = false
		})
		anuncios[index].sel = true;

		//jsonSetOpiniaoVoto
		data = {
			id_anuncio_opiniao: id_pedido,
			id_vestido: anuncios[index].id
		}

		Utils.SimpleLoad("jsonSetOpiniaoVoto.php", data, function(response){
			console.log(response);
			if(response.success){
				
			} else {
				console.log(response);
			}
		});
	}

	$scope.selAba = function(aba){
		$scope.tipo= aba;

		if(aba=="preferidos"){
			Dress.LoadLiked(function(response){
				//console.log(response);
				if(response.success){
					$scope.vestidos = response.data;
				}
			})
		}

		if(aba=="avaliar"){

			$ionicLoading.show();
			data = {
			}
			Utils.SimpleLoad("jsonGetAvaliar.php", data, function(response){
				//console.log(response);
				if(response.success){
					$scope.avaliar = response.data;
				}
				$ionicLoading.hide();
			});

		}

		if(aba=="avaliacoes"){

			$ionicLoading.show();
			data = {
			}
			Utils.SimpleLoad("jsonGetAvaliacaoResultado.php", data, function(response){
				console.log(response);
				if(response.success){
					$scope.avaliacoes = response.data;
				}
				$ionicLoading.hide();
			});

		}
	}

	$scope.status = 'nao-avaliados';
	$scope.selTab = function(tab){
		$scope.status = tab;
	}

	$scope.showAvaliado = function(a){
		if($scope.status=='nao-avaliados'){
			return !a.avaliado;
		} else if($scope.status=='avaliados'){
			return a.avaliado;
		}
	}

	$scope.showMsgAvaliado = function(status){

		avaliado = status=='avaliados';

		qtd = 0;
		angular.forEach($scope.avaliar, function(value){
			if(value.avaliado==avaliado){
				qtd++;
			}
		});

		return $scope.status==status && qtd==0;
	}
	
	if($stateParams.tipo==undefined){
		$scope.selAba('preferidos');
	} else {
		$scope.selAba($stateParams.tipo);
	}

})


.controller('dressCtrl', function($rootScope, $scope, $stateParams, $ionicLoading, $cordovaGeolocation, $ionicSlideBoxDelegate, Dress, Utils, User, $location, $cordovaDialogs) {

	User.Get(function(response){
		$scope.user = response.data;
	});

	$rootScope.$broadcast('changeClass', "dress");

	$scope.vestidos = {};

	$scope.loadDresses = function(){

		moment.locale("pt-br");

		$ionicLoading.show()

		var data = {
			lat: $scope.coords.latitude,
			lng: $scope.coords.longitude,
			id: $stateParams.id_vestido
		}

		Dress.LoadInfo(data, function(response){
			console.log(response);
			if(response.success){
				r = response.data;
				r.usuario.ultimo_login_txt = moment( r.usuario.ultimo_login ).fromNow();
				response.data.cores_hex = response.data.cores_hex.split(",");
				$scope.currentDress = response.data;
				$scope.loadEventos($scope.currentDress.id);
				$ionicSlideBoxDelegate.update(); 
			} else {
				console.log(response);
			}
			$ionicLoading.hide();
		})
	}

	//PEGA A POSICAO DO USUARIO
	var posOptions = {timeout: 10000, enableHighAccuracy: false};
	$ionicLoading.show({
		template: 'Localizando...'
	})
	$cordovaGeolocation.getCurrentPosition(posOptions)
	.then(function (position) {
		$scope.coords  = position.coords;
		$scope.loadDresses();
	}, function(err) {
		$cordovaDialogs.alert("Ative seu GPS para localizar os anúncios mais perto de você!", 'ERRO!', "Ok");
	});

	$scope.startChat = function(id_anuncio){
		data = {
			id_anuncio: id_anuncio
		}

		Dress.StartChat(data, function(response){
			if(response.success){
				$location.path("/app/chat/"+response.id_chat);
			}
		})
	}


	$scope.Edit = function(id_anuncio){
		$location.path("/app/update/"+id_anuncio);
	}


	$scope.eventos = [];
	$scope.loadEventos = function(id){
		data = {
			id_anuncio: id
		}
		Utils.SimpleLoad("jsonGetVestidoEventos.php", data, function(response){
			if(response.success){
				$scope.eventos = response.data;
				console.log($scope.eventos);
			}
		});
	}


	$scope.Dislike = function(){

		$cordovaDialogs.confirm('Tem certeza que deseja remover este anúncio dos preferidos?', 'Remover dos preferidos', ['Sim','Não'])
			.then(function(buttonIndex) {
				if(buttonIndex==1){

					var data = {
						gostou: 'n',
						id_anuncio: $scope.currentDress.id
					}

					$ionicLoading.show();

					Dress.SetLiked(data, function(callback) {

						$ionicLoading.hide();

						if(callback.success){
							//console.log(callback);
							$location.path("/app/liked");
						} else {
							console.log(callback);
						}
					});
				}
			});
	}
  
})


.controller('chatsCtrl', function($rootScope, $scope, $location, Chats) {

	moment.locale("pt-br");

	$rootScope.$broadcast('changeClass', "chats");

	/*$http.get('data/chat.json').success(function(response){ 

		for(i=0; i<response.length; i++){
			response[i].tempo_txt = moment( response[i].tempo ).fromNow();
		}

		$scope.users = response;
	});*/

	$scope.openChat = function(id){
		$location.path("/app/chat/"+id);
	}

	Chats.ListChats(function(response){
		console.log(response);
		if(response.success){
			for(i=0; i<response.data.length; i++){
				response.data[i].tempo_txt = moment( response.data[i].data_cadastro ).fromNow();
			}

			$scope.chats = response.data;
		}
	});

	if($scope.userLogado==undefined) $scope.MsgLogin();

})


.controller('chatCtrl', function($rootScope, $scope, $stateParams, $interval, $timeout, $ionicScrollDelegate, $location, Chats, Utils, User, $ionicModal, $ionicLoading, $cordovaDialogs, MoipService) {

	moment.locale("pt-br");

	User.Get(function(responseUser){
		$scope.userLogado = responseUser.data;
	});

	$rootScope.$broadcast('changeClass', "chat");

	$scope.invokeLoad = true;
	$scope.$on('$destroy', function() {
		$timeout.cancel($scope.interv);
		$interval.cancel($scope.intervTempo);        
		$scope.invokeLoad = false;
	});

	$scope.id_chat = $stateParams.id;
	$scope.conversa = [];

	$scope.DeleteChat = function(){
		$cordovaDialogs.confirm('Tem certeza que deseja remover esse chat?', 'Deletar Chat', ['Deletar','Cancelar'])
		.then(function(buttonIndex) {
			// no button = 0, 'OK' = 1, 'Cancel' = 2
			if(buttonIndex==1){

				var data = {
					id_chat: $scope.id_chat,
					vendedor: $scope.eh_vendedor
				};

				$ionicLoading.show();

				Chats.Delete(data, function(response){
					$ionicLoading.hide();
					console.log(response);
					$location.path("/app/chats");
				});

			}
		});
	}

	$scope.$on('DeleteChat', function() {
		$scope.DeleteChat();
	});

	$scope.initChat = function(){

		data = {
			id_chat: $scope.id_chat
		};

		Chats.LoadChat(data, function(response){
			console.log(response);
			if(response.success){
				$scope.user = response.data.user;
				$scope.produto = response.data.produto;
				$scope.id_anuncio = response.data.id_anuncio;
				$scope.eh_vendedor = response.data.eh_vendedor;
				$scope.pagamento_autorizado = response.data.pagamento_autorizado;
				$scope.pagamento_efetuado = response.data.pagamento_efetuado;
				$scope.pagamento = response.data.pagamento;

				if($scope.pagamento!=undefined){
					$scope.ConsultarPagamento();
				}

				$scope.LoadMsg();
			}
		});
	}

	$scope.$on('SendMsg', function (event, arg) { 
		$scope.chatMsg = arg;
		$scope.SendMsg();
	});

	$scope.SendMsg = function(){

		data = {
			id_chat: $scope.id_chat,
			vendedor: $scope.eh_vendedor,
			msg: $scope.chatMsg,
		};

		$scope.chatMsg = "";

		Chats.SendMsg(data, function(response){
			console.log(response);
			if(response.success){
				//$scope.LoadMsg();
			}
		});
	}

	$scope.LoadMsg = function(){

		$timeout.cancel($scope.interv);

		if($scope.invokeLoad){

			data = {
				id_chat: $scope.id_chat,
				vendedor: $scope.eh_vendedor
			};

			if($scope.conversa.length>0){
				data.min_date = $scope.conversa[$scope.conversa.length-1].data_cadastro;
			}

			Chats.LoadMsg(data, function(response){
				if(response.success){
					for(i=0; i<response.data.length; i++){

						response.data[i].tempo_txt = moment( response.data[i].data_cadastro ).fromNow();

						$scope.conversa.push(response.data[i]);

						if(response.data[i].msg=="PAGAMENTO AUTORIZADO"){
							$scope.pagamento_autorizado = "s";
						}
					}

					if(response.data.length>0)
						$ionicScrollDelegate.scrollBottom(true);

					$scope.interv = $timeout($scope.LoadMsg, 1000);
				}
			});

		}
	}

	$scope.AjustaTempo = function(){
		for(i=0; i<$scope.conversa.length; i++){
			$scope.conversa[i].tempo_txt = moment( $scope.conversa[i].data_cadastro ).fromNow();
		}
	}
	$scope.intervTempo = $interval($scope.AjustaTempo, 1000);


	$scope.AutorizarPagamento = function(){

		console.log($scope.userLogado);
		console.log($scope.userLogado.MoipId);
		if($scope.userLogado.MoipId==""){
			//$scope.OpenModalPayPal();
			$location.path("/meus_dados/moip");
			return false;
		}

		data = {
			id: $scope.id_chat,
			tbl: "chats",
			update: {
				pagamento_autorizado: 's'
			}
		};

		Utils.SimpleUpdate(data, function(response){
			//console.log(response);
			if(response.success){
				$scope.pagamento_autorizado = "s";

				$scope.chatMsg = "PAGAMENTO AUTORIZADO";
				$scope.SendMsg();
			}
		});
	}


	$scope.EfetuarPagamento = function(){
		$location.path("/pagamento/"+$scope.id_anuncio);
	}

	$scope.ConsultarPagamento = function(){
		console.log("Consultando Status");

		MoipService.GET("payments/"+$scope.pagamento.idPagamentoMoip, function(response){

			if(response.data.status=="AUTHORIZED"){
				$scope.infoPagamento = {
					valor: response.data.amount.total/100,
					metodo: "Cartão de Crédito",
					data: moment(response.data.updatedAt).format('LLL')
				}
			}
			
		}, function(response){
			console.log("ERRO");
			console.log(response);
		})

	}
	

	$scope.initChat();
  
}).controller('pagamentoCtrl', function($rootScope, $scope, $stateParams, $ionicLoading, $cordovaDialogs, Dress, Utils, User, $window, MoipService, $location, $timeout, MoipAppData) {

	$scope.back = function(){
		$window.history.back();
	};

	

	$ionicLoading.show();
	$scope.pagamento = {
		nome: "",
		cpf: "",
		data_nascimento: "",
		telefone: "",
		numero: "",
		codigo: "",
		mes: "",
		ano: "",
		publicKey: MoipAppData.publicKey
	};

	$scope.Init = function(){
		User.Get(function(response){
			$scope.usuario = response.data;
			$scope.CarregaProduto();
		})
	}

	//Carregando dados do produto
	$scope.CarregaProduto = function(){
		var data = {
			lat: 0,
			lng: 0,
			id: $stateParams.id
		}
		Dress.LoadInfo(data, function(response){
			if(response.success){

				$scope.anuncio = response.data;
				console.log($scope.anuncio);
				$scope.CarregaPedido();

			} else {
				console.log(response);
			}
			
		});
	}

	$scope.CarregaPedido = function(){
		var data = {
			id_anuncio: $scope.anuncio.id,
			id_vendedor: $scope.anuncio.usuario.id,
			valor: $scope.anuncio.valor,
			id_comprador: $scope.usuario.id
		}

		Utils.SimpleLoad("jsonSetPedido.php", data, function(response){
			console.log(response);
			$scope.pedido = response.data;
			$ionicLoading.hide();

		})
	}


	$scope.CriarPedido = function(){

		$ionicLoading.show();

		var data = {
			ownId: $scope.pedido.id,
			amount: {
				currency: "BRL"
			},
			items: [{
				product: $scope.anuncio.titulo,
				quantity: 1,
				detail: $scope.anuncio.descricao,
				price: $scope.anuncio.valor*100
			}],
			customer: {
				ownId: $scope.usuario.id,
				fullname: $scope.usuario.nome,
				email: $scope.usuario.email,
				birthDate: moment( $scope.pagamento.data_nascimento ).format("YYYY-MM-DD"),
				taxDocument: {
					type: "CPF",
					number: $scope.pagamento.cpf
				},
				phone: {
					countryCode: "55",
					areaCode: $scope.pagamento.telefone.substr(0, 2),
					number: $scope.pagamento.telefone.substr(2),
				}
			},
			receivers: [
			{
				moipAccount: {
					id: "MPA-4B072E76ABD1"
					//login: "dressesilike.brazil@gmail.com"
				},
				type: "PRIMARY",
				amount: {
					percentual: 15
				},
            	"feePayor": true
			},
			{
				moipAccount: {
					id: $scope.pedido.vendedor.MoipId
				},
				type: "SECONDARY",
				amount: {
					percentual: 85
				},
            	"feePayor": false
			}]
		};

		MoipService.POST("orders", data, function(response){
			console.log("SUCESSO");
			console.log(response);
			$scope.SalvarNumPedido(response.data.id);
		}, function(response){
			console.log("ERRO");
			console.log(response);
			$cordovaDialogs.alert("Erro ao gerar seu pedido. Tente novamente mais tarde.", "Erro!", "Ok");
			$ionicLoading.hide();
		})

	}

	$scope.SalvarNumPedido = function(idPedido){

		$ionicLoading.show();

		$scope.pedido.idPedidoMoip = idPedido;

		var data = {
			id: $scope.pedido.id,
			tbl: "pedidos",
			update: {
				idPedidoMoip: idPedido
			}
		}

		Utils.SimpleUpdate(data, function(response){
			console.log(response);
			$scope.ValidarDados();
		});
	}

	$scope.SalvarNumPagamento = function(idPagamento, status){

		if(idPagamento!=undefined)
			$scope.pedido.idPagamentoMoip = idPagamento;

		var data = {
			id: $scope.pedido.id,
			tbl: "pedidos",
			update: {
				idPagamentoMoip: idPagamento,
				status: status
			}
		}

		Utils.SimpleUpdate(data, function(response){
			//$ionicLoading.hide();
			if(idPagamento!=undefined)
				$scope.ConsultarPagamento();
		});
	}


	$scope.EfetuarPagamento = function(){

		$ionicLoading.show();
		console.log("Efetuar Pagamento");

		var data = {
			installmentCount: 1,
			fundingInstrument: {
				method: "CREDIT_CARD",
				creditCard: {
					hash: $scope.cartao.hash(),
					holder: {
						fullname: $scope.pagamento.nome,
						birthdate: moment( $scope.pagamento.data_nascimento ).format("YYYY-MM-DD"),
						taxDocument: {
							type: "CPF",
							number: $scope.pagamento.cpf
						},
						phone: {
							countryCode: "55",
							areaCode: $scope.pagamento.telefone.substr(0, 2),
							number: $scope.pagamento.telefone.substr(2),
						}
					}
				}
			}
		}

		

		MoipService.POST("orders/"+$scope.pedido.idPedidoMoip+"/payments", data, function(response){
			console.log("SUCESSO");
			console.log(response);
			$scope.SalvarNumPagamento(response.data.id,response.data.status);
		}, function(response){
			console.log("ERRO");
			console.log(response);
			$cordovaDialogs.alert(response.errors[0].description, "Erro!", "Ok");
			$ionicLoading.hide();
		})
	}


	$scope.ValidarDados = function(){
		
		$scope.cartao = new Moip.CreditCard({
			number  : $scope.pagamento.numero,
			cvc     : $scope.pagamento.codigo,
			expMonth: $scope.pagamento.mes,
			expYear : $scope.pagamento.ano,
			pubKey  : $scope.pagamento.publicKey
		});

		if($scope.cartao.isValid()){
			$scope.EfetuarPagamento();
		} else {
			$cordovaDialogs.alert("Cartão de Crédito Inválido. Confira seus dados e tente novamente.", "Dados inválidos!", "Ok");
		}

	}


	$scope.ConsultarPagamento = function(){
		console.log("Consultando Status");
		$ionicLoading.show();

		MoipService.GET("payments/"+$scope.pedido.idPagamentoMoip, function(response){
			console.log(response.data);
			if(response.data.status=="AUTHORIZED"){
				$ionicLoading.hide();
				$cordovaDialogs.alert("Pagamento confirmado com sucesso!", "Sucesso!", "Ok");
				$scope.DarBaixaAnuncio();
				$scope.back();
			} else if(response.data.status=="CANCELLED"){
				$cordovaDialogs.alert("O pagamento não foi autorizado pelo sistema de pagamento!", "Erro!", "Ok");
				$ionicLoading.hide();
			} else {
				$timeout($scope.ConsultarPagamento, 5000);
			}

			$scope.SalvarNumPagamento(undefined, response.data.status);

		}, function(response){
			console.log("ERRO");
			console.log(response);
		})

	}

	$scope.DarBaixaAnuncio = function(){

		var data = {
			id_pedido: $scope.pedido.id,
			id_anuncio: $scope.anuncio.id
		}

		Utils.SimpleLoad("jsonSetPedidoFinalizado.php", data, function(response){
			console.log(response);
		});

	}


	$scope.PagarComMoip = function(){

		if($scope.pedido.idPedidoMoip==""){
			console.log("Criar Pedido");
			$scope.CriarPedido();
		} else {
			$scope.ValidarDados();
		}

	}
	
  
	$scope.Init();
})


;



