# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css/"
sass_dir = "css/"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"

output_style = :compressed
environment = :development

relative_assets = true