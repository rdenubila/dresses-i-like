/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.Lazuli.DressesILike;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.Lazuli.DressesILike";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 802;
  public static final String VERSION_NAME = "0.8.2";
}
