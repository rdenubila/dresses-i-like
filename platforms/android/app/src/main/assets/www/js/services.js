angular.module('starter.services', [])


.factory('Foto', function() {
  
	return {

		GetOptions: function(){

			var options = {
				quality: 50,
				destinationType: Camera.DestinationType.DATA_URL,
				sourceType: Camera.PictureSourceType.CAMERA,
				//allowEdit: true,
				encodingType: Camera.EncodingType.JPEG,
				//targetWidth: 800,
				//targetHeight: 800,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};

			return options;
		},

		GetOptionsGallery: function(){

			var options = {
				quality: 50,
				destinationType: Camera.DestinationType.DATA_URL,
				sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
				allowEdit: true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 800,
				targetHeight: 800,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};

			return options;
		}

	}

})


.factory('Config', function($http, $httpParamSerializerJQLike) {

	config = undefined;
  
	return {

		Load: function (callback){

			if(config!=undefined){

				callback(config);

			} else {

				var url = "jsonConfig.php";

				$http({
					method: 'POST',
					url: localStorage.getItem('apiUrl')+url,
					cache: true
				}).success(callback).then(function successCallback(response) {
					config = response.data;
				});

			}

		}

	}

})


.factory('User', function($http, $httpParamSerializerJQLike) {

	$dadosUsuario = undefined;
  
	return {

		Get: function (callback, forcarReload){

			if(forcarReload){
				console.log("Forçando reload");
				$dadosUsuario = undefined;
			}

			if($dadosUsuario!=undefined){

				callback($dadosUsuario);

			} else {

				var url = "jsonLoginToken.php";

				data = {};
				data.token = localStorage.getItem('userToken');

				$http({
					method: 'POST',
					url: localStorage.getItem('apiUrl')+url,
					data: $httpParamSerializerJQLike(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(callback).then(function successCallback(response) {
					$dadosUsuario = response.data;
				});

			}

		},

		Cadastrar: function (data, callback){

			var url = "jsonCadastro.php";

			$dadosUsuario = data;

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url+"?"+$httpParamSerializerJQLike(data),
				cache: true
			}).success(callback).then(function successCallback(response) {
				//console.log(response.data);
			});

		},

		Alterar: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonCadastroEditar.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response.data);
			});

		},

		Login: function (data, callback){

			var url = "jsonLogin.php";

			console.log(data);

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				$dadosUsuario = response.data;
			});

		},

		LoginByToken: function (data, callback){

			var url = "jsonLoginToken.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				$dadosUsuario = response.data;
			});

		},

		Validar: function (data, callback){

			var url = "jsonValidar.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url+"?"+$httpParamSerializerJQLike(data),
				cache: true
			}).success(callback).then(function successCallback(response) {
				//$dadosUsuario = response.data.data;
			});

		},

		LoginFb: function (data, callback){

			var url = "jsonLoginFacebook.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url+"?"+$httpParamSerializerJQLike(data),
				cache: false
			}).success(callback).then(function successCallback(response) {
				//$dadosUsuario = response.data.data;
			});

		},

		LogOut: function (callback){

			localStorage.clear();
			callback(true);

		}

	}

})


.factory('Dress', function($http, $httpParamSerializerJQLike) {
  
	return {

		Add: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonCadastrarAnuncio.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response.data);
			});

		},

		Load: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetVestidos.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		LoadInfo: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetVestidoInfo.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		LoadLiked: function (callback){

			data = {};

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetVestidosLiked.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		SetLiked: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonSetUsuarioVestido.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		LoadAnuncios: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetAnuncios.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		UpdateAnuncios: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonUpdateAnuncio.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		StartChat: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonStartChat.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		}

	}

})


.factory('Utils', function($http, $httpParamSerializerJQLike) {
  
	return {

		GetLongLat:  function (address, callback){

			var url = "http://maps.googleapis.com/maps/api/geocode/json?address="+address;

			$http({
				url: url
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		GetCategories: function(){
			if(localStorage.getItem('catsSel')){
				return localStorage.getItem('catsSel').split(",");
			} else {
				return [];
			}
		},

		GetEstConserv: function(){
			if(localStorage.getItem('estConserv')){
				return localStorage.getItem('estConserv').split(",");
			} else {
				return [];
			}
		},

		GetMaxDist: function(){
			if(localStorage.getItem('maxDist')){
				return parseInt(localStorage.getItem('maxDist'));
			} else {
				return 100;
			}
		},

		GetTextByArray: function(array){
			t = [];
	        for(i=0; i<array.length; i++){
	        	if(array[i]!=undefined)
	            	t.push(array[i].nome);
	        }
	        return t.join(", ");
		},

		SearchInArray: function(array, id){
			for(k=0; k<array.length; k++){
				if(array[k].id==id){
					return array[k];
				}
			}
		},

		SimpleInsert: function (data, callback){

			url = "jsonSimpleInsert.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback);

		},

		SimpleUpdate: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonSimpleUpdate.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		SimpleLoad: function (url, data, callback){

			data.token = localStorage.getItem('userToken');

			var url = url;

			console.log($httpParamSerializerJQLike(data));

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				cache: false,
			}).success(callback).error(function (error){ console.error(error)});

		},

		SimpleDelete: function (data, callback){

			url = "jsonSimpleDelete.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback);
			
		}

	}

})


.factory('Chats', function($http, $httpParamSerializerJQLike) {

	return {

		ListChats: function (callback){

			data = {};
			data.token = localStorage.getItem('userToken');

			var url = "jsonGetChatList.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		LoadChat: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetChat.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},

		LoadMsg: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonGetChatMsg.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},


		SendMsg: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonSendChatMsg.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		},


		Delete: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonDeleteChat.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		}

	}

})


.factory('MoipService', function($http, $httpParamSerializerJQLike, MoipAppData) {

	var urlMoip = "https://sandbox.moip.com.br/v2/";
	var urlMoipConnect = "https://connect-sandbox.moip.com.br/";
	var BASE64 = "MU9JT1pBQktCSDRDOTZPWldNWVVTTDlJWkdJTE9EMTk6OUNZWEJZOVVCSU5CMERGSjZGUTFSVkJWVjhTRlIzWVpOR0NJMjdSNQ==";


	return {

		POST: function (url, data, responseSuccess, responseError){

			var headers = {
				'Content-Type': 'application/json',
				'Authorization': 'OAuth '+MoipAppData.accessToken
			};

			$http({
				method: 'POST',
				responseType: "json",
				url: urlMoip+url,
				data: JSON.stringify(data),
				headers: headers
			}).then(function successCallback(response) {
				responseSuccess(response);
			}, function errorCallback(response) {
				responseError(response.data);
			});

		},

		POST_URL: function (url, data, responseSuccess, responseError){

			var headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'Basic '+BASE64,
				'Cache-Control': "no-cache"
			};
			console.log(headers);
			console.log(data);

			$http({
				method: 'POST',
				url: urlMoipConnect+url,
				data: $httpParamSerializerJQLike(data),
				headers: headers
			}).then(function successCallback(response) {
				responseSuccess(response);
			}, function errorCallback(response) {
				responseError(response);
			});

		},

		GET: function (url, responseSuccess, responseError){

			var headers = {
				'Content-Type': 'application/json',
				'Authorization': 'OAuth '+MoipAppData.accessToken
			};

			$http({
				method: 'GET',
				responseType: "json",
				url: urlMoip+url,
				headers: headers
			}).then(function successCallback(response) {
				responseSuccess(response);
			}, function errorCallback(response) {
				responseError(response);
			});

		}

	}

})

.factory('PayPal', function($http, $httpParamSerializerJQLike) {

	var urlPayPal = "https://api.sandbox.paypal.com/v1/";
	var clientId = "AeF025fh4siiWZoUWgd7k3kpCK0PT1s2Fk0VGVnn9Z9sCHfeExTQ6t7IFsYKqtl6XT6qO2uJMgZ9UmrA";
	var secret = "EJHp5b2nTJc7XUnjOzwdevb87zfXZ5GkW37H7uUlvV4xcmyrV3CHPy7HZ-Z76PgSQsXqGo71xKVUDKp2";

	var tokenPayPal = undefined;


	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


	return {

		Do: function (url, data, callback){

			data.token = localStorage.getItem('userToken');

			headers = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer '+tokenPayPal
			};

			data.token = undefined;

			console.log(headers);
			console.log(data);

			$http({
				method: 'POST',
				url: urlPayPal+url,
				data: JSON.stringify(data),
				headers: headers
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			}, function errorCallback(response) {
				console.log(response);
			});

		},

		GetToken: function (callback){

			if(tokenPayPal != undefined){
				callback(tokenPayPal);
			} else {


				headers = {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization': 'Basic '+Base64.encode(clientId+":"+secret),
				};
				//console.log(headers);

				data = {
					grant_type: "client_credentials"
				}

				$http({
					method: 'POST',
					url: urlPayPal+"oauth2/token",
					data: $httpParamSerializerJQLike(data),
					headers: headers
				}).then(function successCallback(response) {
					tokenPayPal = response.data.access_token;
					callback(tokenPayPal);
				}, function errorCallback(response) {
					alert("Erro ao gerar Token de Pagamento")
					console.log(response);
				});

			}

		}

	}

})


/*.factory('Pagamento', function($http, $httpParamSerializerJQLike) {

	return {

		EfetuarPagamento: function (data, callback){

			data.token = localStorage.getItem('userToken');

			var url = "jsonEfetuarPagamento.php";

			$http({
				method: 'POST',
				url: localStorage.getItem('apiUrl')+url,
				data: $httpParamSerializerJQLike(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(callback).then(function successCallback(response) {
				//console.log(response);
			});

		}

	}

})*/






;
