var ref;
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('appDIL', ['ionic', 'starter.controllers', 'starter.services', 'gajus.swing', 'ngCordova','ui.utils.masks', 'mdo-angular-cryptography'])

.run(function($ionicPlatform, $rootScope) {

  $ionicPlatform.ready(function() {

	/*Keyboard.shrinkView(true);
	
	if(ionic.Platform.platform()=="ios"){
	  Keyboard.shrinkView(true);
	}*/

	var TratarNoficacao = function(jsonData) {
		data = jsonData.notification.payload.additionalData;
		//console.log(data);
		$rootScope.$broadcast('Notificacao', data);
	};

  if(window.plugins!=undefined){
	window.plugins.OneSignal
		.startInit("5511633d-e0c5-4a34-91d7-6743476355b6", "413250867111")
		.handleNotificationOpened(function(jsonData) { console.log("OPENED"); TratarNoficacao(jsonData); })
		.handleNotificationReceived(function(jsonData) { console.log("RECEIVED"); TratarNoficacao(jsonData); })
		.endInit();
  }

	// Sync hashed email if you have a login system or collect it.
	//   Will be used to reach the user at the most optimal time of day.
	// window.plugins.OneSignal.syncHashedEmail(userEmail);
		

	if(window.cordova && window.cordova.plugins.Keyboard) {
	  // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	  // for form inputs)
	  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

	  // Don't remove this line unless you know what you are doing. It stops the viewport
	  // from snapping when text inputs are focused. Ionic handles this internally for
	  // a much nicer keyboard experience.
	  //cordova.plugins.Keyboard.disableScroll(true);
	  cordova.plugins.Keyboard.disableScroll(false);

	}

	if(window.StatusBar) {
	  //StatusBar.styleDefault(1);
	}
	
  });
})


.constant("MoipAppData", {
	id: "APP-G9SNABX6MJO6",
	website: "http://www.dressesilike.com.br/Sistema/api/templates/LoginMoip.php",
	accessToken: "2178b919e9c9471199240c4e90c6329b_v2",
	name: "Dresses I Like",
	secret: "dcadf9b938f74efcb8c50ff6cc739c5e",
	redirectUri: "http://www.dressesilike.com.br/Sistema/api/templates/LoginMoip.php",
	publicKey: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiEvYbf/jdOk4EWcgJ7Ei\n9JoBSScGPuRo6/N2Y5n98EFOxNBIqrktYp4Jk6aToxRMGKVbuaZVH9SLqCe2TF/B\nzwMW4f36R68EpyPmbTODdz+BZSNs/N+9RYunc7qgvbP+YBh0las90izBZdt9P5z9\ntYRv4nTlM+Z4GGXPcHjWAPMTAhJ/8nNO1RUPkjYSzfBNIXSD42WRxqM3EBZQ9uF+\nnpaOY7rg10McRsmeeuLpHQ+Zlb0BXIn/SVfND9vIWsXvSYwfsLP4YN2Vj2G0E5ka\nEFLcMehl+8fsaJldiYiFx4ceNffo1v8a1AUu0/Ya/fmG8jHONeDRpNNnVugdHTns\nRwIDAQAB\n-----END PUBLIC KEY-----"
})


.config(function($stateProvider, $urlRouterProvider, $cryptoProvider, $httpProvider) {

  ionic.Platform.setPlatform('android');

  $cryptoProvider.setCryptographyKey('DIL147');

  //apiUrl = "http://192.168.0.12/Dresses_I_Like/Sistema/api/";
  apiUrl = "http://www.dressesilike.com.br/Sistema/api/";

  localStorage.setItem('apiUrl', apiUrl);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // Each tab has its own nav history stack:

  .state('splash', {
	url: '/splash',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/splash.html',
		controller: 'splashCtrl'
	  }
	}
  })

  .state('instructions', {
	url: '/instructions',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/instructions.html',
		controller: 'instructionsCtrl'
	  }
	}
  })

  .state('entrar', {
	url: '/entrar',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/entrar.html',
		controller: 'loginCtrl'
	  }
	}
  })

  .state('login', {
	url: '/login',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/login.html',
		controller: 'loginCtrl'
	  }
	}
  })

  .state('esqueciSenha', {
	url: '/esqueciSenha',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/esqueci_senha.html',
		controller: 'EsqueciSenhaCtrl'
	  }
	}
  })

  .state('cadastro', {
	url: '/cadastro',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/cadastro.html',
		controller: 'signInCtrl'
	  }
	}
  })

  .state('meus_dados', {
	url: '/meus_dados',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/meus_dados.html',
		controller: 'signInCtrl'
	  }
	}
  })


  .state('meus_dados2', {
	url: '/meus_dados/:variacao',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/meus_dados.html',
		controller: 'signInCtrl'
	  }
	}
  })

  .state('validacao', {
	url: '/validacao',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/validacao.html',
		controller: 'validateCtrl'
	  }
	}
  })

  .state('pagamento', {
	url: '/pagamento/:id',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/pagamento.html',
		controller: 'pagamentoCtrl'
	  }
	}
  })

  .state('app', {
	url: '/app',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  }
	}
  })

  .state('app.home', {
	url: '/home',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/home.html',
		controller: 'homeCtrl'
	  }
	}
  })

  .state('app.add', {
	url: '/add',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/add.html',
		controller: 'addCtrl'
	  }
	}
  })

  .state('app.update', {
	url: '/update/:id',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/add.html',
		controller: 'addCtrl'
	  }
	}
  })

  .state('app.user', {
	url: '/user',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/user.html',
		controller: 'userCtrl'
	  }
	}
  })

  .state('app.user_interna', {
	url: '/user/:tipo',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/user.html',
		controller: 'userCtrl'
	  }
	}
  })

  .state('app.chats', {
	url: '/chats',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/chats.html',
		controller: 'chatsCtrl'
	  }
	}
  })

  .state('app.chat', {
	url: '/chat/:id',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/chat.html',
		controller: 'chatCtrl'
	  }
	}
  })

  .state('app.liked', {
	url: '/liked',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/liked.html',
		controller: 'likedCtrl'
	  }
	}
  })

  .state('app.liked_interna', {
	url: '/liked/:tipo',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/liked.html',
		controller: 'likedCtrl'
	  }
	}
  })

  .state('app.dress', {
	url: '/dress/:id_vestido',
	views: {
	  'viewGeral': {
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	  },
	  'viewApp': {
		templateUrl: 'templates/dress.html',
		controller: 'dressCtrl'
	  }
	}
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/splash');

});
